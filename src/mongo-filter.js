module.exports.params = async({
    query,
    transform,
    mapKey = {},
    allow_keys = {},
    specialConditionFunc,
    deny = ["sort_type", "sort_by", "limit", "skip", "sort"],
    op = ["in", "gt", "gte", "eq", "lte", "lt", "nin", "ne"]
}) => {
    let condition = {};
    let pattern = `/^((.+)[_](or)[_]([a-z]+))|((.+)[_](${op.sort(function(a, b){return b.length - a.length}).join("|")}))|([a-zA-Z_.]+)$/`;
    let allow_field = Object.keys(allow_keys);
    for (let key of Object.keys(query)) {
        let _t, _f, _v;
        _m = key.match(new RegExp(eval(pattern)));
        if (!_m)
            continue;
        _f = (_m && _m[3] == 'or') ? _f = _m[2] : (_m && _m[7]) ? _m[6] : _m[8];
        _p = (_m && _m[3] == 'or') ? _m[4] : (_m && _m[7]) ? _m[7] : 'eq';
        if (mapKey && mapKey[_p])
            _p = mapKey[_p];
        if (!allow_field.includes(_f) || deny.includes(_f) || !op.includes(_p) || !allow_keys[_f].includes(_p))
            continue;
        _v = (transform && transform[_f]) ? transform[_f](query[key]) : query[key];
        if (_m[3] == 'or') {
            _op = {
                [`$or`]: condition.$or || []
            };
            let _e;
            for (let item of _op.$or)
                if (Object.keys(item).includes(_f))
                    _e = item;
            _t = {
                [`\$${_p}`]: _v
            };
            if (!_e)
                _op[`$or`].push({
                    [`${_f}`]: _t
                });
            else
                Object.assign(_e[`${_f}`], _t);
            Object.assign(condition, _op);
            continue;
        };
        if (_m[8]) {
            condition[_f] = _v;
            continue;
        }
        _t = {
            [`\$${_p}`]: _v
        };
        if (condition[_f])
            Object.assign(condition[_f], _t)
        else
            condition[`${_f}`] = _t;
    };
    if (specialConditionFunc)
        Object.assign(condition, specialConditionFunc({ condition, query }));
    return condition;
};

module.exports.sortBy = ({ query, allow_keys }) => {
    let _s = {};
    "sort_type", "sort_by"
    if (!query["sort_by"])
        return _s;
    let _t = query.sort_by;
    let allow_fields = Object.keys(allow_keys);
    if (allow_fields.includes(_t)) {
        if (query["sort_type"] == 1)
            query["sort_type"] = "desc"
        if (query["sort_type"] == -1)
            query["sort_type"] = "asc"
        _s = {
            [_t]: query["sort_type"]
        };
    }
    return _s;
};

module.exports.sortMultipleField = ({ query, allow_keys, sortType = ["asc", "desc", 1, -1] }) => {
    let _s = {};
    if (!query["sort"])
        return _s;
    let _t = query.sort;
    let allow_fields = Object.keys(allow_keys);
    for (let sort of _t.split(",")) {
        let op = sort.split(":");
        let _f = op[0];
        let _op = !isNaN(op[1]) ? op[1] * 1 : op[1];
        if (!allow_keys)
            continue;
        if (allow_keys[_f] && !allow_keys[_f].includes(_op))
            continue;
        if (allow_fields.includes(_f) && sortType.includes(_op))
            _s[_f] = _op
    };
    return _s;
}