const { sortBy, params, sortMultipleField } = require("./src/mongo-filter");

exports.filterFunc = function(req, res, next) {
    req.filter = { sortBy, params, sortMultipleField };
    next()
}